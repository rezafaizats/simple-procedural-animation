﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IKFootPlacement
{
    [RequireComponent(typeof(Animator))]
    public class IKRetargeting : MonoBehaviour
    {
        [Header("Foot Retargeting")]
        private Vector3 rightFootPos, leftFootPos, rightFootIKPos, leftFootIKPos;
        private Quaternion rightFootIKRot, leftFootIKRot;
        private float lastPelvisPosY, lastRightFootPosY, lastLeftFootPosY;
        private Animator playerAnim;

        [Header("Foot IK Options")]
        public bool enableFeetIK = true;
        [Range(0, 2)] [SerializeField] private float heightFromGroundRaycast = 1.14f;
        [Range(0, 2)] [SerializeField] private float raycastDownDistance = 1.5f;
        [SerializeField] private LayerMask environmentLayer;
        [SerializeField] private float pelvisOffset = 0.1f;
        [Range(0, 1)] [SerializeField] private float pelvisUpDownSpeed = 0.28f;
        [Range(0, 1)] [SerializeField] private float feetToIKPositionSpeed = 0.5f;

        public string leftFootAnimVariableName = "LeftFootCurve";
        public string rightFootAnimVariableName = "RightFootCurve";
        public bool showSolverDebug = true;

        public Transform leftToe, rightToe;

        void Start()
        {
            playerAnim = GetComponent<Animator>();
        }

        private void FixedUpdate()
        {
            if (!enableFeetIK || playerAnim == null)
                return;

            AdjustFeetTarget(ref rightFootPos, HumanBodyBones.RightFoot);
            AdjustFeetTarget(ref leftFootPos, HumanBodyBones.LeftFoot);

            FootPositionSolver(rightFootPos, ref rightFootIKPos, ref rightFootIKRot);
            FootPositionSolver(leftFootPos, ref leftFootIKPos, ref leftFootIKRot);

        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (!enableFeetIK || playerAnim == null)
                return;

            MovePelvisHeight();
            playerAnim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
            playerAnim.SetIKRotationWeight(AvatarIKGoal.RightFoot, playerAnim.GetFloat(rightFootAnimVariableName));

            MoveFootToIKPoint(AvatarIKGoal.RightFoot, rightFootIKPos, rightFootIKRot, ref lastRightFootPosY);

            playerAnim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
            playerAnim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, playerAnim.GetFloat(leftFootAnimVariableName));

            MoveFootToIKPoint(AvatarIKGoal.LeftFoot, leftFootIKPos, leftFootIKRot, ref lastLeftFootPosY);

            ToePlacement(playerAnim, 0.15f, 0.14f, leftToe, rightToe, environmentLayer);
        }

        private void MoveFootToIKPoint(AvatarIKGoal foot, Vector3 positionIKHolder, Quaternion rotationIKHolder, ref float lastFootPositionY)
        {
            Vector3 targetIKPos = playerAnim.GetIKPosition(foot);

            if (positionIKHolder != Vector3.zero)
            {
                targetIKPos = transform.InverseTransformPoint(targetIKPos);
                positionIKHolder = transform.InverseTransformPoint(positionIKHolder);

                float yVar = Mathf.Lerp(lastFootPositionY, positionIKHolder.y, feetToIKPositionSpeed);
                targetIKPos.y += yVar;

                lastFootPositionY = yVar;
                targetIKPos = transform.TransformPoint(targetIKPos);
                playerAnim.SetIKRotation(foot, rotationIKHolder);
            }

            playerAnim.SetIKPosition(foot, targetIKPos);

        }

        private void MovePelvisHeight()
        {
            if (rightFootIKPos == Vector3.zero || leftFootIKPos == Vector3.zero || lastPelvisPosY == 0)
            {
                lastPelvisPosY = playerAnim.bodyPosition.y;
                return;
            }

            float leftOffsetPos = leftFootIKPos.y - transform.position.y;
            float rightOffsetPos = rightFootIKPos.y - transform.position.y;

            float totalOffset = (leftOffsetPos < rightOffsetPos) ? leftOffsetPos : rightOffsetPos;

            Vector3 newPelvisPos = playerAnim.bodyPosition + Vector3.up * totalOffset;
            newPelvisPos.y = Mathf.Lerp(lastPelvisPosY, newPelvisPos.y, pelvisUpDownSpeed);

            playerAnim.bodyPosition = newPelvisPos;
            lastPelvisPosY = playerAnim.bodyPosition.y;
        }

        private void FootPositionSolver(Vector3 fromSkyPosition, ref Vector3 feetIKPosition, ref Quaternion feetIKRotation)
        {
            RaycastHit feetOutHit;

            if (showSolverDebug)
                Debug.DrawLine(fromSkyPosition, fromSkyPosition + Vector3.down * (raycastDownDistance + heightFromGroundRaycast), Color.yellow);

            if (Physics.Raycast(fromSkyPosition, Vector3.down, out feetOutHit, raycastDownDistance + heightFromGroundRaycast, environmentLayer))
            {
                feetIKPosition = fromSkyPosition;
                feetIKPosition.y = feetOutHit.point.y + pelvisOffset;
                feetIKRotation = Quaternion.FromToRotation(Vector3.up, feetOutHit.normal) * transform.rotation;

                return;
            }

            feetIKPosition = Vector3.zero;

        }

        private void AdjustFeetTarget(ref Vector3 feetPosition, HumanBodyBones foot)
        {
            feetPosition = playerAnim.GetBoneTransform(foot).position;
            feetPosition.y = transform.position.y + heightFromGroundRaycast;
        }

        public void ToePlacement(Animator animator, float HeelOffset, float FeetOffset, Transform L_Toe, Transform R_Toe, LayerMask Mask)
        {
            Vector3 T_Pos; Quaternion T_Rot;
            //
            animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, (animator.pivotWeight < 0.51f) ? 1f : 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, (animator.pivotWeight < 0.51f) ? 1f : 0f);
            animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, (animator.pivotWeight > 0.49f) ? 1f : 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, (animator.pivotWeight > 0.49f) ? 1f : 0f);
            //
            RaycastHit LYH, RYH, LZH, RZH;
            Transform Hips = animator.GetBoneTransform(HumanBodyBones.Hips);
            Ray L_Y = new Ray(new Vector3(animator.GetIKPosition(AvatarIKGoal.LeftFoot).x, Hips.position.y, animator.GetIKPosition(AvatarIKGoal.LeftFoot).z), new Vector3(0, -1, 0));
            Ray R_Y = new Ray(new Vector3(animator.GetIKPosition(AvatarIKGoal.RightFoot).x, Hips.position.y, animator.GetIKPosition(AvatarIKGoal.RightFoot).z), new Vector3(0, -1, 0));
            if (Physics.Raycast(L_Y, out LYH, Mask.value))
            {
                Vector3 Y = new Vector3(LYH.point.x, LYH.point.y + HeelOffset, LYH.point.z);
                animator.SetIKPosition(AvatarIKGoal.LeftFoot, Y);
                if (animator.GetBoneTransform(HumanBodyBones.LeftFoot).position.y < Y.y)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1f);
                }
                Debug.DrawLine(Hips.position, L_Y.origin, Color.cyan);
                Debug.DrawLine(L_Y.origin, LYH.point, Color.green);
                Vector3 L_T = new Vector3(L_Toe.position.x, animator.GetIKPosition(AvatarIKGoal.LeftFoot).y, L_Toe.position.z);
                Vector3 L_Feet = new Vector3(L_T.x, L_T.y + HeelOffset * 2f, L_T.z);
                Ray L_Z = new Ray(L_Feet, new Vector3(0, -1, 0));
                if (Physics.Raycast(L_Z, out LZH, Mask.value))
                {
                    T_Pos = animator.GetIKPosition(AvatarIKGoal.LeftFoot);
                    T_Rot = animator.GetIKRotation(AvatarIKGoal.LeftFoot);
                    Vector3 FeetTarget = new Vector3(LZH.point.x, LZH.point.y + FeetOffset, LZH.point.z);
                    T_Rot.SetLookRotation(FeetTarget - T_Pos, LYH.normal);
                    animator.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.Slerp(animator.GetIKRotation(AvatarIKGoal.LeftFoot), T_Rot, Time.time));
                    Debug.DrawLine(animator.GetIKPosition(AvatarIKGoal.LeftFoot), L_Feet, Color.cyan);
                    Debug.DrawLine(L_Feet, LZH.point, Color.green);
                }
            }
            //
            if (Physics.Raycast(R_Y, out RYH, Mask.value))
            {
                Vector3 Y = new Vector3(RYH.point.x, RYH.point.y + HeelOffset, RYH.point.z);
                animator.SetIKPosition(AvatarIKGoal.RightFoot, Y);
                if (animator.GetBoneTransform(HumanBodyBones.RightFoot).position.y < Y.y)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1f);
                }
                Debug.DrawLine(Hips.position, R_Y.origin, Color.cyan);
                Debug.DrawLine(R_Y.origin, RYH.point, Color.green);
                Vector3 R_T = new Vector3(R_Toe.position.x, animator.GetIKPosition(AvatarIKGoal.RightFoot).y, R_Toe.position.z);
                Vector3 R_Feet = new Vector3(R_T.x, R_T.y + HeelOffset * 2f, R_T.z);
                Ray R_Z = new Ray(R_Feet, new Vector3(0, -1, 0));
                if (Physics.Raycast(R_Z, out RZH, Mask.value))
                {
                    T_Pos = animator.GetIKPosition(AvatarIKGoal.RightFoot);
                    T_Rot = animator.GetIKRotation(AvatarIKGoal.RightFoot);
                    Vector3 FeetTarget = new Vector3(RZH.point.x, RZH.point.y + FeetOffset, RZH.point.z);
                    T_Rot.SetLookRotation(FeetTarget - T_Pos, RYH.normal);
                    animator.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.Slerp(animator.GetIKRotation(AvatarIKGoal.RightFoot), T_Rot, Time.time));
                    Debug.DrawLine(animator.GetIKPosition(AvatarIKGoal.RightFoot), R_Feet, Color.cyan);
                    Debug.DrawLine(R_Feet, RZH.point, Color.green);
                }
            }
        }
    }

}
