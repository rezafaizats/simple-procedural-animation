﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IKFootPlacement
{
    [RequireComponent(typeof(CharacterController))]
    public class ThirdPersonHumanoidController : MonoBehaviour
    {
        #region Variables
        [Header("Player Input")]
        [SerializeField] private Transform _mainCamera;
        [SerializeField] private float _characterSpeed;
        private Vector2 _characterDirection;
        private CharacterController _characterController;

        [Header("Ground Check")]
        [SerializeField] private float _gravitySpeed;
        public float duration = 0.08f;
        private float startTime;
        private bool counterStarted = false;
        public bool isGrounded = false;

        public RaycastHit hit;
        public float distance = 1.2f;
        public Vector3 direction = new Vector3(0f, -1f, 0f);
        #endregion

        // Start is called before the first frame update
        void Start()
        {
            if(_mainCamera == null)
                _mainCamera = Camera.main.transform;

            _characterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            _characterDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            isGrounded = IsGrounded();
        }

        private void FixedUpdate()
        {
            if (isGrounded)
                MovePlayer(_characterDirection);
            else
                Gravity();
        }

        public void MovePlayer(Vector2 direction)
        {
            if (direction.magnitude != 0f)
            {
                float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + _mainCamera.eulerAngles.y;
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDirection = Quaternion.Euler(0f, angle, 0f) * Vector3.forward;
                _characterController.Move(moveDirection.normalized * _characterSpeed * Time.deltaTime);
            }
        }

        public float CountTime()
        {
            return Time.time - startTime;
        }

        public bool IsGrounded()
        {
            return IsGroundedByCController() || IsGroundedByRaycast();
        }

        public bool IsGroundedByCController()
        {
            CharacterController controller = GetComponent<CharacterController>();
            if (controller.isGrounded == false)
            {
                if (counterStarted == false)
                {
                    startTime = Time.time;
                    counterStarted = true;
                }
            }
            else counterStarted = false;

            if (CountTime() > duration)
            {
                return false;
            }
            return true;
        }

        public bool IsGroundedByRaycast()
        {
            Debug.DrawRay(transform.position, direction * distance, Color.green);

            if (Physics.Raycast(transform.position, direction, out hit, distance))
                return true;

            return false;
        }

        public void Gravity()
        {
            if (!isGrounded)
            {
                Vector3 characterDirection = Vector3.down * _gravitySpeed * Time.deltaTime;
                _characterController.Move(characterDirection);
            }
        }

    }

}
