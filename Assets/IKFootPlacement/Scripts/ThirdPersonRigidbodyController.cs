﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IKFootPlacement
{
    public class ThirdPersonRigidbodyController : MonoBehaviour
    {
        #region Variables
        [Header("Character Options")]
        [SerializeField] private Transform _mainCamera;
        [SerializeField] private float _characterSpeed = 4f;
        [SerializeField] private float _characterJumpPower = 2f;
        private Vector2 _characterDirection;
        private Rigidbody _characterRb;

        [Header("Character Animations")]
        [SerializeField] private bool isUsingAnimation;
        [SerializeField] private string walkingAnimationName;
        [SerializeField] private string jumpingAnimationName;
        private Animator _characterAnimator;

        #endregion

        // Start is called before the first frame update
        void Start()
        {
            if (_mainCamera == null)
                _mainCamera = Camera.main.transform;

            _characterRb = GetComponent<Rigidbody>();

            if (isUsingAnimation)
                _characterAnimator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            _characterDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            if (Input.GetKeyDown(KeyCode.Space))
                JumpCharacter();
        }

        private void FixedUpdate()
        {
            MoveCharacter(_characterDirection);
        }

        public void MoveCharacter(Vector2 direction)
        {
            if (direction.magnitude != 0f)
            {
                float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + _mainCamera.eulerAngles.y;
                _characterRb.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDirection = Quaternion.Euler(0f, angle, 0f) * Vector3.forward;
                //_characterRb.velocity = new Vector3(moveDirection.x * _characterSpeed * Time.deltaTime, _characterRb.velocity.y, moveDirection.x * _characterSpeed * Time.deltaTime);
                _characterRb.MovePosition(transform.position + (moveDirection.normalized * _characterSpeed * Time.deltaTime));

                if (isUsingAnimation)
                    _characterAnimator.SetBool(walkingAnimationName, true);
            }
            else
            {
                if (isUsingAnimation)
                    _characterAnimator.SetBool(walkingAnimationName, false);
            }
        }

        public void JumpCharacter()
        {
            //_characterRb.velocity = new Vector3(_characterRb.velocity.x, 2f, _characterRb.velocity.z);
            _characterRb.MovePosition(transform.position + (Vector3.up * _characterJumpPower * Time.deltaTime));

            if (isUsingAnimation)
                _characterAnimator.SetTrigger(jumpingAnimationName);
        }

    }
}
